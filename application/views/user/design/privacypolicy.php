<body id="page-details" class="loaded">
    <div class="page-wrapper">        
        <main class="main">
             <div class="container mt-3 mb-3">
                <ol class="breadcrumb mt-0 mb-2">
                    <li class="breadcrumb-item"><a href="<?= base_url() ?>"><i class="icon-home"></i></a></li>
                    <li class="breadcrumb-item"><a href="#" class="text-active">Privacy Policy</a></li>
                </ol>

               <div class="row row-sm">

                <div class="col-md-12">
                     <h3> Information Collection And Use</h3>
                       <p>While using our Service, we may ask you to provide us with certain personally identifiable information that can be used to contact or identify you. Personally identifiable information may include, but is not limited to, your name, phone number, postal address, other information ("Personal Information").
                       </p>
                </div>

              <div class="col-md-12">
                    <h3>Log Data</h3>
                       <p>We collect information that your browser sends whenever you visit our Service ("Log Data"). This Log Data may include information such as your computer's Internet Protocol ("IP") address, browser type, browser version, the pages of our Service that you visit, the time and date of your visit, the time spent on those pages and other statistics.
                       </p>
              </div>

              <div class="col-md-12">
                    <h3>Cookies</h3>
                       <p>Cookies are files with small amount of data, which may include an anonymous unique identifier. Cookies are sent to your browser from a web site and stored on your computer's hard drive.<br>

                      We use "cookies" to collect information. You can instruct your browser to refuse all cookies or to indicate when a cookie is being sent. However, if you do not accept cookies, you may not be able to use some portions of our Service.
                                             </p>
              </div>

              <div class="col-md-12">
                    <h3>Service Providers</h3>
                       <p>We may employ third party companies and individuals to facilitate our Service, to provide the Service on our behalf, to perform Service-related services or to assist us in analyzing how our Service is used.
                       <br>
                      These third parties have access to your Personal Information only to perform these tasks on our behalf and are obligated not to disclose or use it for any other purpose.
                       </p>
              </div>


               <div class="col-md-12">
                    <h3>Security</h3>
                       <p>The security of your Personal Information is important to us, but remember that no method of transmission over the Internet, or method of electronic storage is 100% secure. While we strive to use commercially acceptable means to protect your Personal Information, we cannot guarantee its absolute security.
                       </p>
              </div>

              <div class="col-md-12">
                    <h3>Links To Other Sites</h3>
                       <p>Our Service may contain links to other sites that are not operated by us. If you click on a third party link, you will be directed to that third party's site. We strongly advise you to review the Privacy Policy of every site you visit.<br>

                      We have no control over, and assume no responsibility for the content, privacy policies or practices of any third party sites or services.
                       </p>
              </div>

              <div class="col-md-12">
                    <h3>Children's Privacy</h3>
                       <p>Our Service does not address anyone under the age of 13 ("Children").
                          <br>
                        We do not knowingly collect personally identifiable information from children under 13. If you are a parent or guardian and you are aware that your Children has provided us with Personal Information, please contact us. If we discover that a Children under 13 has provided us with Personal Information, we will delete such information from our servers immediately.
                       </p>
              </div>

              <div class="col-md-12">
                    <h3>Changes To This Privacy Policy</h3>
                       <p>We may update our Privacy Policy from time to time. We will notify you of any changes by posting the new Privacy Policy on this page.<br>

                      You are advised to review this Privacy Policy periodically for any changes. Changes to this Privacy Policy are effective when they are posted on this page.
                       </p>
              </div>

              <div class="col-md-12">
                    <h3>Contact Us</h3>
                       <p>If you have any questions about this Privacy Policy, please contact us.
                       </p>
              </div>



              </div><!--row-->
              
              
                        
            </div> <!-----container---->   