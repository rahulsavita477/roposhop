<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>Roposhop</title>

    <meta name="" content="HTML5 Template" />
    <meta name="" content="P">
    <meta name="" content="">
        
       <!---css--->
         <?php include('include/css.php'); ?>
       <!---css--->

       <style type="text/css" media="screen">

        html {
  scroll-behavior: smooth;
}
         .product-default:hover figure {
          box-shadow:none;
} 
#scs button.owl-prev, #scs button.owl-next {
    width: 20px !important;
    height: 20px !important;
    background:none !important;
}
.rating-left {
    float: left;
    position: absolute;
    top: 5px;
    left: 5px;
    font-size: 10px;
    color: #fff;
    padding: 2px;
}
a.text-active {
    color: #08c;
}
.color-change {
    color: #08c !important;
}
.sidebar-shop .widget {
    margin: 0;
    padding: 2.3rem 8px 1.8rem;
  }

.owl-item .cloned a:hover{
  text-decoration: none;
}
       </style>
</head>
<body id="page-details">
    <div class="page-wrapper">
        
        <!---header--->
        <?php include('include/header.php'); ?> 
        <!-- End .header -->

        <main class="main">

         <div class="container">
                <ol class="breadcrumb mt-0 mb-2">
                        <li class="breadcrumb-item"><a href="index.php"><i class="icon-home"></i></a></li>
                        <li class="breadcrumb-item"><a href="Details.php">product Details</a></li>
                        <li class="breadcrumb-item"><a href="#" class="text-active">Listing Information</a></li>
                    </ol>


                    
                <div class="product-single-container product-single-default">
                   <div class="row">
            <div class="col-md-5">
                <div class="sticky-slider">
                                <div class="product-slider-container product-item">
                                    <div class="product-single-carousel owl-carousel">
                                        <div class="product-item">
                                            <img class="product-single-image" src="assets/images/products/zoom/product-1.jpg" data-zoom-image="assets/images/products/zoom/product-1-big.jpg">
                                        </div>
                                        <div class="product-item">
                                            <img class="product-single-image" src="assets/images/products/zoom/product-2.jpg" data-zoom-image="assets/images/products/zoom/product-2-big.jpg">
                                        </div>
                                        <div class="product-item">
                                            <img class="product-single-image" src="assets/images/products/zoom/product-3.jpg" data-zoom-image="assets/images/products/zoom/product-3-big.jpg">
                                        </div>
                                          <div class="product-item">
                                            <img class="product-single-image" src="assets/images/products/zoom/product-4.jpg" data-zoom-image="assets/images/products/zoom/product-4-big.jpg">
                                        </div>
                                          <div class="product-item">
                                            <img class="product-single-image" src="assets/images/products/zoom/product-5.jpg" data-zoom-image="assets/images/products/zoom/product-5-big.jpg">
                                        </div>
                                        

                                    </div>
                                    <!-- End .product-single-carousel -->
                                    <span class="prod-full-screen">
                                        <i class="icon-plus"></i>
                                    </span>
                                </div>

                                <div class="prod-thumbnail row owl-dots transparent-dots" id='carousel-custom-dots'>
                                    <div class="owl-dot">
                                        <img src="assets/images/products/zoom/product-1.jpg">
                                    </div>
                                    <div class="owl-dot">
                                         <img src="assets/images/products/zoom/product-2.jpg">
                                    </div>
                                    <div class="owl-dot">
                                        <img src="assets/images/products/zoom/product-3.jpg">
                                    </div>
                                    <div class="owl-dot">
                                        <img src="assets/images/products/zoom/product-4.jpg">
                                    </div>

                                     <div class="owl-dot">
                                        <img src="assets/images/products/zoom/product-5.jpg">
                                    </div>
                                   
                                </div>
                            </div>
            </div><!-----5---->

            <div class="col-md-7">
              <h2 class="product">Samsung Galaxy A7  (64GB)  (4GB RAM)</h2>
                 <span class="color-change pt-3 pb-3"> <strong><strike>₹ 13999</strike>
                                    &nbsp; ₹ 1000
                     </strong>
                     33.2%&nbsp; Off  &nbsp; (Discount &nbsp; ₹ 4520)
                     </span>
              <div class="row pt-3 mt-5">
                  <div class="col-md-5">
                      <h4>Rating</h4>
               
                        <h1 class="rating-num">
                            4.0 <i class="fa fa-star text-dark"></i></h1>
                        
                        <div>
                            <span>63,238</span> Ratings &amp;
                            <span> 6,397</span> Reviews
                        </div>
                  </div>
                  <div class="col-md-7">
                    <div class="pull-left">
                    <div class="pull-left" style="width:35px; line-height:1;">
                        <div style="height:9px; margin:5px 0;">5 <span class="fa fa-star"></span></div>
                    </div>
                    <div class="pull-left" style="width:320px;">
                        <div class="progress" style="height:9px; margin:8px 0;">
                          <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="5" aria-valuemin="0" aria-valuemax="5" style="width: 1000%;background: #5cb85c">
                            <span class="sr-only">80% Complete (danger)</span>
                          </div>
                        </div>
                    </div>
                    <div class="pull-right" style="margin-left:10px;">1</div>
                </div>
                <div class="pull-left">
                    <div class="pull-left" style="width:35px; line-height:1;">
                        <div style="height:9px; margin:5px 0;">4 <span class="fa fa-star"></span></div>
                    </div>
                    <div class="pull-left" style="width:320px;">
                        <div class="progress" style="height:9px; margin:8px 0;">
                          <div class="progress-bar progress-bar-primary" role="progressbar" aria-valuenow="4" aria-valuemin="0" aria-valuemax="5" style="width: 80%;background: #428bca">
                            <span class="sr-only">80% Complete (danger)</span>
                          </div>
                        </div>
                    </div>
                    <div class="pull-right" style="margin-left:10px;">1</div>
                </div>
                <div class="pull-left">
                    <div class="pull-left" style="width:35px; line-height:1;">
                        <div style="height:9px; margin:5px 0;">3 <span class="fa fa-star"></span></div>
                    </div>
                    <div class="pull-left" style="width:320px;">
                        <div class="progress" style="height:9px; margin:8px 0;">
                          <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="3" aria-valuemin="0" aria-valuemax="5" style="width: 60%;background:#5bc0de">
                            <span class="sr-only">80% Complete (danger)</span>
                          </div>
                        </div>
                    </div>
                    <div class="pull-right" style="margin-left:10px;">0</div>
                </div>
                <div class="pull-left">
                    <div class="pull-left" style="width:35px; line-height:1;">
                        <div style="height:9px; margin:5px 0;">2 <span class="fa fa-star"></span></div>
                    </div>
                    <div class="pull-left" style="width:320px;">
                        <div class="progress" style="height:9px; margin:8px 0;">
                          <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="2" aria-valuemin="0" aria-valuemax="5" style="width: 40%;background:#f0ad4e">
                            <span class="sr-only">80% Complete (danger)</span>
                          </div>
                        </div>
                    </div>
                    <div class="pull-right" style="margin-left:10px;">0</div>
                </div>
                <div class="pull-left">
                    <div class="pull-left" style="width:35px; line-height:1;">
                        <div style="height:9px; margin:5px 0;">1 <span class="fa fa-star"></span></div>
                    </div>
                    <div class="pull-left" style="width:320px;">
                        <div class="progress" style="height:9px; margin:8px 0;">
                          <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="1" aria-valuemin="0" aria-valuemax="5" style="width: 20%;background:#d9534f">
                            <span class="sr-only">80% Complete (danger)</span>
                          </div>
                        </div>
                    </div>
                    <div class="pull-right" style="margin-left:10px;">0</div>
                </div>
           
                  </div>            
              </div><!---row--->  
            </div> 
             </div>
               </div>


               <div class="container">
                 <div class="row">

                  <div class="col-md-9">
                    <h2>Offered By</h2><br>
                    <div class="">
                     <img src="assets/images/merchants/4.JPEG" style="height:100px; width: auto;float:left;">  <h3 class="pl-5 d-flex flex-column justify-content-center">Gaurav Enterprises
                      </h3>
                    </div>
                    <table class="table pt-3">
                                        <tbody>
                                        <tr>
                                          <td>Sale Price</td>
                                            <td class="text-left">₹ 99999.00 (44.44% Off)</td>
                                          </tr>
                                          <tr>
                                           <td>Finance available</td>
                                           <td class="text-left"> No</td>
                                            </tr>
                                          <tr>
                                          <td>Finance terms</td>
                                          <td class="text-left"><span class="text-primary">
                                            <a href="ajax/product-quick-view.php" class="btn-quickview"><strong>T&C</strong> </a></span>
                                          </td>
                                          </tr>

                                          <tr>
                                          <td>Home delievery available</td>
                                          <td class="text-left">No</td>
                                          </tr>

                                          <tr>
                                          <td>Home delivery terms</td>
                                          <td class="text-left"><a href="ajax/product-quick-view.php" class="btn-quickview"><strong>T&C</strong> </a></td>
                                          </tr>

                                          <tr>
                                          <td>Installation available</td>
                                          <td class="text-left">No</td>
                                          </tr>

                                          <tr>
                                          <td>Installation terms</td>
                                          <td class="text-left"><a href="ajax/product-quick-view.php" class="btn-quickview"><strong>T&C</strong> </a></td>
                                          </tr>

                                          <tr>
                                          <td>Installation terms</td>
                                          <td class="text-left">Yes</td>
                                          </tr>

                                          <tr>
                                          <td>Replacement available/td>
                                          <td class="text-left">No</td>
                                          </tr>

                                          <tr>
                                          <td>Replacement terms</td>
                                          <td class="text-left">No</td>
                                          </tr>


                                 </tbody>
                             </table>
                            
                             <div class="pt-2 pb-2">
                             <h2 class="pt-5 step-title">Offer(s) provided by merchant on product</h2>

                             <strong>Merhant address</strong>
                          <p>142-143, REVENUE NAGAR, Annapurna Road, NEAR DUSSHERA MAIDAN, Indore, Madhya Pradesh, India - 452009
                          Shop no: 9152769319</p>
                          </div>


                             <div class="pt-2 pb-2">
                             <h2 class="pt-5 step-title">Business Days</h2>

                             <strong>All day</strong>
                          <p></p>
                          </div>

                          
                             <div class="pt-2 pb-2">
                             <h2 class="pt-5 step-title">Business Hours</h2>

                             <strong>10:30 am - 9:30 pm</strong>
                          <p></p>
                          </div>

                  </div>



                 <!-------->
                 <div class="sidebar-shop col-lg-3 mobile-sidebar" id="scs">

                     <div class="widget widget-featured">
                                <h3 class="widget-title">Featured Products</h3>
                                
                                <div class="widget-body">
                                    <div class="owl-carousel widget-featured-products owl-loaded owl-drag">
                                        <!-- End .featured-col -->

                                        <!-- End .featured-col -->
                                    <div class="owl-stage-outer owl-height" style="height: 297px;"><div class="owl-stage" style="transform: translate3d(-516px, 0px, 0px); transition: all 0.25s ease 0s; width: 1548px;">

                                    <div class="owl-item cloned" style="width: 258px;">
                                     <?php for ($i=1; $i <=1; $i++){?>
                            
                                     <div class="featured-col">
                                        <a href="details.php"> 
                                     <div class="product-site left-details product-widget" style="padding:15px 5px 5px ">
                                        <span class="rating-left" style="background:#28a745">
                                           2&nbsp;<i class="fa fa-star"></i> (1) 
                                           </span><!-- End .ratings -->
                                        <figure>
                                        <img src="assets/images/products/product-12.jpg">
                                        </figure>
                                        <div class="product-details">
                                        <h2 class="product-title text-black" style="font-size: 13px;font-weight: 500">
                                          Redmi Y3 (64 GB) (4 GB RAM)....<br>
                                         <strong><strike>₹ 13999</strike>&nbsp; ₹ 1000<br></strong>
                                         33.2%&nbsp; Off  &nbsp;
                                         </h2>
                                         </div><!-- End .product-details -->
                                         
                                        </div>
                                        </a>
                                    </div>
                                     <?php } ?>

                                     <div class="featured-col">
                                        <a href="details.php"> 
                                     <div class="product-site left-details product-widget" style="padding:15px 5px 5px ">
                                        <span class="rating-left" style="background:#28a745">
                                           2&nbsp;<i class="fa fa-star"></i> (1) 
                                           </span><!-- End .ratings -->
                                        <figure>
                                        <img src="assets/images/products/product-12.jpg">
                                        </figure>
                                        <div class="product-details">
                                        <h2 class="product-title text-black" style="font-size: 13px;font-weight: 500">
                                          Redmi Y3 (64 GB) (4 GB RAM)....<br>
                                         <strong><strike>₹ 13999</strike>&nbsp; ₹ 1000<br></strong>
                                         33.2%&nbsp; Off  &nbsp;
                                         </h2>
                                         </div><!-- End .product-details -->
                                         
                                        </div>
                                        </a>
                                    </div>
                                    </div>
                                        <div class="owl-item active" style="width: 258px;"><div class="featured-col">

                                           
                                              <div class="owl-item cloned" style="width: 258px;">
                                     <?php for ($i=1; $i <=3; $i++){?>
                            
                                     <div class="featured-col">
                                        <a href="details.php"> 
                                    <div class="product-site left-details product-widget" style="padding:15px 5px 5px ">
                                        <span class="rating-left" style="background:#28a745">
                                           2&nbsp;<i class="fa fa-star"></i> (1) 
                                           </span><!-- End .ratings -->
                                        <figure>
                                        <img src="assets/images/products/product-12.jpg">
                                        </figure>
                                        <div class="product-details">
                                        <h2 class="product-title text-black" style="font-size: 13px;font-weight: 500">
                                          Redmi Y3 (64 GB) (4 GB RAM)....<br>
                                         <strong><strike>₹ 13999</strike>&nbsp; ₹ 1000<br></strong>
                                         33.2%&nbsp; Off  &nbsp;
                                         </h2>
                                         </div><!-- End .product-details -->
                                         
                                        </div>
                                        </a>
                                    </div>
                                     <?php } ?>
                                    </div>
                                           
                                          
                                        </div>
                                    </div>

                                        <div class="owl-item" style="width: 258px;"><div class="featured-col">

                                           <div class="owl-item cloned" style="width: 258px;">
                                     <?php for ($i=1; $i <=5; $i++){?>
                            
                                     <div class="featured-col">
                                        <a href="details.php"> 
                                     <div class="product-site left-details product-widget" style="padding:15px 5px 5px ">
                                        <span class="rating-left" style="background:#28a745">
                                           2&nbsp;<i class="fa fa-star"></i> (1) 
                                           </span><!-- End .ratings -->
                                        <figure>
                                        <img src="assets/images/products/product-12.jpg">
                                        </figure>
                                        <div class="product-details">
                                        <h2 class="product-title text-black" style="font-size: 13px;font-weight: 500">
                                          Redmi Y3 (64 GB) (4 GB RAM)....<br>
                                         <strong><strike>₹ 13999</strike>&nbsp; ₹ 1000<br></strong>
                                         33.2%&nbsp; Off  &nbsp;
                                         </h2>
                                         </div><!-- End .product-details -->
                                         
                                        </div>
                                        </a>
                                    </div>
                                     <?php } ?>
                                    </div>

                                        </div>
                                    </div>

                                
                                </div>
                            </div>

                                        <div class="owl-dots disabled "></div></div><!-- End .widget-featured-slider -->
                                </div><!-- End .widget-body -->
                            </div><!-- End .widget -->
                     
                 </div>
                <!-------->


                 </div><!--row-->
               </div> <!---container---->



        </main><!-- End .main -->

        
        <!----Start footer-->
        <?php include('include/footer.php'); ?>
        <!-- End .footer -->
    </div><!-- End .page-wrapper -->

    <div class="mobile-menu-overlay"></div><!-- End .mobil-menu-overlay -->

       <!--Mobile-menu-container-->
           <?php include('include/mobile-menu.php'); ?>
       <!-- End .mobile-menu-container -->

   

   
    

    <a id="scroll-top" href="#top" role="button"><i class="icon-angle-up"></i></a>

       <!----------js-------->
       <?php include('include/js.php'); ?>
       
       <!---------=Js-------->
</body>

</html>


