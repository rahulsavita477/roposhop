<!-- Plugins JS File -->
<script src="<?= $this->config->item('site_url').'assets/user/assets2/js/jquery.min.js' ?>"></script>
<script src="<?= $this->config->item('site_url').'assets/user/assets2/js/bootstrap.bundle.min.js' ?>"></script>
<script src="<?= $this->config->item('site_url').'assets/user/assets2/js/plugins.min.js' ?>"></script>

<!-- Main JS File -->
<script src="<?= $this->config->item('site_url').'assets/user/assets2/js/main.min.js' ?>"></script>