 <div class="mobile-menu-container">
        <div class="mobile-menu-wrapper">
            <span class="mobile-menu-close"><i class="icon-cancel"></i></span>
            <nav class="mobile-nav">
                <ul class="mobile-menu">
                    <li class="active "><a href="index.php">Home</a></li>
                            
                           
                           <li><a href="#" class="sf-with-ul">Mobile Phones</a>
                                <ul>
                                    <li><a href="#">All in Mobile Phones</a></li>
                                    <li><a href="#">Smart Phones</a></li>
                                    <li><a href="#">Smart Wearable Tech</a></li>
                                    <li><a href="#">Mobile Accessories</a></li>

                                </ul>
                            </li>
                            <li><a href="#" class="sf-with-ul">TVs & Home Entertainment</a>
                                <ul>
                                   <li><a href="#">All in TVs & Home Entertainment</a></li>
                                   <li><a href="#">Televisions</a></li>
                                </ul>
                            </li> 

                             <li><a href="#" class="sf-with-ul">Home Appliances
                                </a>
                                <ul>
                                     <li><a href="#">All in Home Appliances</a></li>
                                     <li><a href="#">Water Geysers</a></li>
                                     <li><a href="#">Refrigerators</a></li>
                                     <li><a href="#">Air Conditioners</a></li>
                                     <li><a href="#">Washing Machines</a></li>
                                     <li><a href="#">Kitchen Appliances</a></li>
                                     <li><a href="#">Small Home Appliances</a></li>
                                     <li><a href="#">Cooler</a></li>
                                 </ul>
                              </li>

                              <li><a href="#" class="sf-with-ul">Computers</a>
                                  <ul>
                                 <li><a href="#">All in Computers</a></li>
                                 <li><a href="#">Laptops</a></li>
                                 <li><a href="#">Desktop PCs</a></li>
                                 <li><a href="#">Computer Peripherals & Accessories</a></li>
                                  </ul>
                              </li> 

                               <li><a href="#" class="sf-with-ul">Industrial Equipments</a>
                                 <ul>
                                <li><a href="#">All in Industrial Equipments</a></li>
                                <li><a href="#">Fabrication</a></li>
                                <li><a href="#">PEB Components</a></li>
                                <li><a href="#">Industrial Tools</a></li>
                                <li><a href="#">Industrial Accessories</a></li>
                                </ul>
                              </li>         
                            
                    
                </ul>
            </nav><!-- End .mobile-nav -->

            <div class="social-icons">
                <a href="#" class="social-icon" target="_blank"><i class="icon-facebook"></i></a>
                <a href="#" class="social-icon" target="_blank"><i class="icon-twitter"></i></a>
                <a href="#" class="social-icon" target="_blank"><i class="icon-instagram"></i></a>
            </div><!-- End .social-icons -->
        </div><!-- End .mobile-menu-wrapper -->
    </div>