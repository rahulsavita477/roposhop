<script type="text/javascript">
    WebFontConfig = {
        google: { families: [ 'Open+Sans:300,400,600,700,800','Poppins:300,400,500,600,700','Segoe Script:300,400,500,600,700' ] }
    };
    (function(d) {
        var wf = d.createElement('script'), s = d.scripts[0];
        wf.src = '<?= $this->config->item('site_url')."assets/user/assets2/js/webfont.js" ?>';
        wf.async = true;
        s.parentNode.insertBefore(wf, s);
    })(document);
</script>

<!-- Plugins CSS File -->
<link rel="stylesheet" href="<?= $this->config->item('site_url').'assets/user/assets2/css/bootstrap.min.css' ?>" />

<!-- Main CSS File -->
<link rel="stylesheet" href="<?= $this->config->item('site_url').'assets/user/assets2/css/style.min.css' ?>" />
<link rel="stylesheet" type="text/css" href="<?= $this->config->item('site_url').'assets/user/assets2/vendor/fontawesome-free/css/all.min.css' ?>" />
<link rel="stylesheet" type="text/css" href="<?= $this->config->item('site_url').'assets/user/assets2/css/custom.css' ?>" />