<body id="page-details" class="loaded">
    <div class="page-wrapper">        
        <main class="main">
             <div class="container mt-3 mb-3">
                <ol class="breadcrumb mt-0 mb-2">
                    <li class="breadcrumb-item"><a href="<?= base_url() ?>"><i class="icon-home"></i></a></li>
                    <li class="breadcrumb-item"><a href="#" class="text-active">About us</a></li>
                </ol>

               <div class="row row-sm">

                <div class="col-md-12" id="contactus">
                     <h2>Contact us</h2>
                       <p>While using our Service, we may ask you to provide us with certain personally identifiable information that can be used to contact or identify you. Personally identifiable information may include, but is not limited to, your name, phone number, postal address, other information ("Personal Information").
                       </p>
                </div>

              <div class="col-md-12" id="help&support">
                    <h3>Help & Support</h3>
                       <p>We collect information that your browser sends whenever you visit our Service ("Log Data"). This Log Data may include information such as your computer's Internet Protocol ("IP") address, browser type, browser version, the pages of our Service that you visit, the time and date of your visit, the time spent on those pages and other statistics.
                       </p>
              </div>

              <div class="col-md-12" id="t&c">
                    <h3>Term & Condition</h3>
                       <p>Cookies are files with small amount of data, which may include an anonymous unique identifier. Cookies are sent to your browser from a web site and stored on your computer's hard drive.<br>

                      We use "cookies" to collect information. You can instruct your browser to refuse all cookies or to indicate when a cookie is being sent. However, if you do not accept cookies, you may not be able to use some portions of our Service.
                                             </p>
              </div>

              <div class="col-md-12" id="faq">
                    <h3>FAQs</h3>
                       <p>We may employ third party companies and individuals to facilitate our Service, to provide the Service on our behalf, to perform Service-related services or to assist us in analyzing how our Service is used.
                       <br>
                      These third parties have access to your Personal Information only to perform these tasks on our behalf and are obligated not to disclose or use it for any other purpose.
                       </p>
              </div>
              </div><!--row-->
              
              
                        
            </div> <!-----container---->   